# README #

The Cov2Finder repository contains a python script covariancefinder.py, which
can align raw reads to the genome (https://www.ncbi.nlm.nih.gov/nuccore/NC_045512), or to the annotated genes of the SARS-CoV-2. It also reports a matrix of the Single Nucleotides population that are different from the genome.

This server is still under development.

## Content of the repository ##

* covariancefinder.py : The python3 script

## Installation ##

Setting up Cov2Finder script and database
```bash
# Go to wanted location for cov2finder
cd /path/to/some/dir

# Clone and enter the cov2finder directory
git clone https://git@bitbucket.org/genomicepidemiology/cov2finder.git
cd cov2finder


# Installing up the Cov2Finder database
# Go to wanted location for cov2finder database
cd /path/to/some/dir

# Clone and enter the cov2finder directory
git clone https://git@bitbucket.org/genomicepidemiology/cov2finder_db.git
cd cov2finder_db
```
### Installing dependencies:

If kma_index has no bin install please install kma_index from the kma repository:
https://bitbucket.org/genomicepidemiology/kma

Install the cgecore module to python3
```bash
pip3 install cgecore
```

## Usage

The program can be invoked with the -h option to get help and more information of the service.

```bash
# Example of running cov2finder
python3 covariancefinder.py -i /path/to/raw_read1 /path/to/raw_read2 -o . -p /path/to/cov2finder_db -mp /path/to/kma -t 0.20 -l 0.20

# Options of cov2finder
usage: covariancefinder.py [-h] [-i INFILE [INFILE ...]] [-o OUTDIR]
                           [-tmp TMP_DIR] [-mp METHOD_PATH] [-p DB_PATH]
                           [-op DB_OP] [-l MIN_COV] [-t IDENTITY_THRESHOLD]
                           [-d DEPTH_THRESHOLD] [-c MIN_ABUNDANCE] [-x] [-q]

optional arguments:
  -h, --help            show this help message and exit
  -i INFILE [INFILE ...], --infile INFILE [INFILE ...]
                        FASTA or FASTQ input files.
  -o OUTDIR, --outputPath OUTDIR
                        Path for output files
  -tmp TMP_DIR, --tmp_dir TMP_DIR
                        Temporary directory for storage of the results from
                        the external software.
  -mp METHOD_PATH, --methodPath METHOD_PATH
                        Path to executable kma
  -p DB_PATH, --databasePath DB_PATH
                        Path to the databases
  -op DB_OP, --databaseOption DB_OP
                        Choose between genes-covid and genome-covid
  -l MIN_COV, --mincov MIN_COV
                        Minimum coverage
  -t IDENTITY_THRESHOLD, --identityThreshold IDENTITY_THRESHOLD
                        Minimum threshold for identity
  -d DEPTH_THRESHOLD, --depthThreshold DEPTH_THRESHOLD
                        Minimum threshold for depth
  -c MIN_ABUNDANCE, --minAbundance MIN_ABUNDANCE
                        Minimum abundance of mutation for reporting
  -x, --extented_output
                        Give extented output with allignment files, template
                        and query hits in fasta and a tab seperated file with
                        allele profile results
  -q, --quiet
```

### Web-server

A webserver implementing the methods is available at the [CGE
website](http://www.genomicepidemiology.org/) and can be found here:
https://cge.cbs.dtu.dk/services/cov2finder/


License
=======

Copyright (c) 2014, Ole Lund, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
